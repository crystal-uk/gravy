<?php
namespace crystal\gravy\services;

use Craft;
use yii\base\Component;

class Assets extends Component {

  public function volumes() {
    return Craft::$app->volumes->getAllVolumes();
  }

}
