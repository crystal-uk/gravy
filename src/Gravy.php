<?php
namespace crystal\gravy;

use crystal\gravy\Variables;

use Craft;
use craft\base\Plugin;
// use craft\events\RegisterUrlRulesEvent;
// use craft\web\UrlManager;
use craft\web\twig\variables\CraftVariable;

use yii\base\Event;


class Gravy extends Plugin {
  public static $plugin;

  public function init() {

    parent::init();
    self::$plugin = $this;

    $this->setComponents([
      'assets' => \crystal\gravy\services\Assets::class,
    ]);

    // // Register our CP routes
    // Event::on(UrlManager::class, UrlManager::EVENT_REGISTER_CP_URL_RULES,
    //   function (RegisterUrlRulesEvent $event) {
    //     $event->rules[$rl] = ['template' => 'gravy/name'];
    //   }
    // );

    // Register our variables
    Event::on(CraftVariable::class, CraftVariable::EVENT_INIT,
      function (Event $event) {
        $variable = $event->sender;
        $variable->set('gravy', Variables::class);
      }
    );

    Craft::info(Craft::t('gravy', '{name} plugin loaded', ['name' => $this->name]), __METHOD__);
  }
}
