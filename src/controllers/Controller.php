<?php
namespace crystal\gravy\controllers;

use crystal\gravy\Gravy;
use crystal\gravy\models\UserModel;
use crystal\gravy\records\UserRecord;

use Craft;
use craft\web\Controller;


class AssetsController extends Controller {

  public function actionDelete() {
    $id = Craft::$app->getRequest()->getBodyParam('assetId');

    if (Craft::$app->assets->deleteAsset($id)) {
      Craft::$app->getSession()->setNotice('Asset deleted');
      return $this->redirectToPostedUrl();
    } else {
      Craft::$app->getSession()->setError('Asset not found');
    }
  }

}
