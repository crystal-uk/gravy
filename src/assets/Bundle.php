<?php
namespace crystal\gravy\assets;

use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

class Bundle extends AssetBundle {
  public function init() {
    $this->sourcePath = '@crystal/gravy/../resources';

    $this->depends = [CpAsset::class];

    $this->js  = ['scripts.js'];
    $this->css = ['styles.css'];

    parent::init();
  }
}
