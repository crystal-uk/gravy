<?php
namespace crystal\gravy;

use crystal\gravy\Gravy;

use Craft;
use craft\elements\Asset;

class Variables {

  public function volumes() {
    return Gravy::$plugin->assets->volumes();
  }

  public function assets($volume) {
    return Asset::find()->volume($volume)->orderBy('folderId')->all();
  }

  public function folder($folder) {
    return Craft::$app->assets->getFolderById($folder);
  }

}
